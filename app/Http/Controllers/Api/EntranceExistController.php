<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CameraDataLogger;
use App\Models\Employee;
use App\Models\TestDataCheck;
use App\Models\TimeSchedule;
use Carbon\Carbon;
use FFI\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EntranceExistController extends Controller
{
    public function aiCamApiData(Request $request)
    {

        if ($request->header('cpsdai-secret') === 'Cpsd@I@td&') :
            if ($request->filled('rand_string') && $request->filled('base64') && $request->filled('camera_id')) :

                if (strlen($request->rand_string) === 16) :

                    $aiCamDatas = trim(file_get_contents("php://input"));

                    TestDataCheck::create([
                        'data_text'     => 'camera data request',
                        'data_json'     => $aiCamDatas
                    ]);

                    $randString = $request->rand_string;

                    CameraDataLogger::create([
                        'rand_string'   => $randString,
                        'camera_id'     => $request->camera_id,
                        'base64'        => $request->base64,
                    ]);

                    $localAiServer  = env('LOCAL_AI_SERVER');
                    $empRecogSlug   = env('EMP_RECOG_API_SLUG');
                    $faceShowServer = env('MONITOR_API_SERVER');

                    try {
                        $aiResponse = Http::post($localAiServer . '/' . $empRecogSlug, [
                            'task'          => 'RECOGNIATION',
                            'file'          => $request->base64,
                            'token'         => $randString,
                            'company'       => env('COMPANY_SLUG'),
                        ]);

                        TestDataCheck::create([
                            'data_text'     => 'server response after face cogniation',
                            'data_json'     => $aiResponse
                        ]);

                        $aiResponseArray = $aiResponse->json();

                        if ($aiResponseArray['token'] == $randString) :
                            if ($aiResponseArray['status'] == 200 && $aiResponseArray['recognition'] == true) :
                                $camInfo = CameraDataLogger::where('rand_string', $randString)->first();
                                $cameraId = $camInfo->camera_id;
                                $camInfo = $camInfo->delete();
                                $employeeInfo = Employee::where('employee_id', '=', $aiResponseArray['user_id'])->where('employee_status', 1)->first();

                                if ($employeeInfo !== null) :
                                    if ($cameraId == 1) :
                                        $employeeEntranceResult = $this->employeeEntrance($aiResponseArray['user_id'], $employeeInfo->designation_id, $aiResponse);

                                        $empImagePath = storage_path('app/public/images/') . $employeeInfo->employee_image;
                                        $empImgBase64 = base64_encode(file_get_contents($empImagePath));

                                        if ($employeeEntranceResult['status'] == 200) :
                                            Http::withHeaders([
                                                'Accept:application/json',
                                                'Content-Type:application/json;charset=utf-8',
                                                'cpsdai_secret' => '@Rejohn@Hi',
                                            ])->post($faceShowServer, [
                                                'name'          => $employeeInfo->employee_name,
                                                'message'       => 'Welcome Back',
                                                'image'         => $empImgBase64,
                                            ]);
                                        endif;

                                        if ($employeeEntranceResult['status'] == 201) :
                                            Http::withHeaders([
                                                'Accept:application/json',
                                                'Content-Type:application/json;charset=utf-8',
                                                'cpsdai_secret' => '@Rejohn@Hi',
                                            ])->post($faceShowServer, [
                                                'name'          => $employeeInfo->employee_name,
                                                'message'       => 'Welcome',
                                                'image'         => $empImgBase64,
                                            ]);
                                        endif;

                                        return response($employeeEntranceResult);
                                    endif;

                                    if ($cameraId == 2) :
                                        $employeeOutResult = $this->employeeOut($aiResponseArray['user_id'], $employeeInfo->designation_id, $aiResponse);

                                        $empImagePath = storage_path('app/public/images/') . $employeeInfo->employee_image;
                                        $empImgBase64 = base64_encode(file_get_contents($empImagePath));

                                        if ($employeeOutResult['status'] == 201) :
                                            Http::withHeaders([
                                                'Accept:application/json',
                                                'Content-Type:application/json;charset=utf-8',
                                                'cpsdai_secret' => '@Rejohn@Hi',
                                            ])->post($faceShowServer, [
                                                'name'          => $employeeInfo->employee_name,
                                                'message'       => 'Good Bye',
                                                'image'         => $empImgBase64,
                                            ]);
                                        endif;

                                        return response($employeeOutResult);
                                    endif;
                                else :
                                    $data = array(
                                        'status' => 404,
                                        'message' => 'User Not Found'
                                    );
                                    return response($data);
                                endif;
                            endif;
                        else :
                            CameraDataLogger::where('rand_string', $randString)->update(['rand_string' => NULL]);
                            $data = array(
                                'status' => 203,
                                'message' => 'Non-authoritative Information'
                            );
                            return response($data);
                        endif;
                    } catch (Exception $error) {
                        $data = array(
                            'status' => 500,
                            'message' => 'Internal Server Error'
                        );
                        return response($data);
                    }

                else :
                    $data = array(
                        'status' => 400,
                        'message' => 'Bad Request'
                    );
                    return response($data);
                endif;
            else :
                $data = array(
                    'status' => 204,
                    'message' => 'No Content'
                );
                return response($data);
            endif;
        else :
            $data = array(
                'status' => 401,
                'message' => 'Unauthorized Request'
            );
            return response($data);
        endif;
    }

    public function employeeEntrance($userId, $designationId, $aiDatas)
    {
        $presentDate = Carbon::now()->format('Y-m-d');
        $entranceTime = $this->checkEntranceDataExit($userId, $presentDate);

        if ($entranceTime === null) :
            $presentTime = Carbon::now()->format('H:i:s');
            $saveEntranceTime = $this->addEntranceTime($userId, $designationId, $presentDate, $presentTime, $aiDatas);

            if ($saveEntranceTime) {
                $data = array(
                    'status' => 201,
                    'message' => 'Employee Entrance.'
                );
                return $data;
            } else {
                $data = array(
                    'status' => 417,
                    'message' => 'Expectation Failed'
                );
                return $data;
            }
        else :
            $data = array(
                'status' => 200,
                'message' => 'Already Added'
            );
            return $data;
        endif;
    }

    public function employeeOut($userId, $designationId, $aiDatas)
    {
        $presentDate = Carbon::now()->format('Y-m-d');
        $entranceTime = $this->checkEntranceDataExit($userId, $presentDate);

        if ($entranceTime === null) :
            $data = array(
                'status' => 406,
                'message' => 'Not Acceptable'
            );
            return $data;
        else :
            $presentTime = Carbon::now()->format('H:i:s');
            $saveOutTime = $this->addOutTime($userId, $presentDate, $presentTime, $aiDatas);

            if ($saveOutTime) {
                $data = array(
                    'status' => 201,
                    'message' => 'Employee Exit.'
                );
                return $data;
            } else {
                $data = array(
                    'status' => 417,
                    'message' => 'Expectation Failed'
                );
                return $data;
            }
        endif;
    }

    public function checkEntranceDataExit($userId, $date)
    {
        $entranceTime = TimeSchedule::select('entrance_time')->where('employee_id', $userId)->where('date', $date)->orderBy('id', 'DESC')->first();
        return $entranceTime;
    }

    public function addEntranceTime($userId, $designationId, $presentDate, $presentTime, $aiDatas)
    {
        $entranceTime = new TimeSchedule;

        $entranceTime->employee_id          = $userId;
        $entranceTime->designation_id       = $designationId;
        $entranceTime->date                 = $presentDate;
        $entranceTime->entrance_time        = $presentTime;
        $entranceTime->api_entrance_data    = $aiDatas;

        $saveEntranceTime = $entranceTime->save();
        return $saveEntranceTime;
    }

    public function addOutTime($userId, $presentDate, $presentTime, $aiDatas)
    {
        $updateOutTime = TimeSchedule::where('employee_id', $userId)->where('date', $presentDate)->orderBy('id', 'DESC')->first();

        $updateOutTime->out_time        = $presentTime;
        $updateOutTime->api_out_data    = $aiDatas;

        $saveOutTime = $updateOutTime->save();
        return $saveOutTime;
    }
}
