<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\TimeSchedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TimeScheduleController extends Controller
{
    public function aiApiData(Request $request)
    {
        if ($request->header('cpsdai-secret') === 'Cpsd@I@td&') :
            $aiDatas = trim(file_get_contents("php://input"));

            if ($request->filled('user_id') && $request->filled('entrance_time') && $request->filled('out_time')) :
                if ($request->entrance_time === true && $request->out_time === true) :
                    $data = array(
                        'status' => 400,
                        'message' => 'Bad Request'
                    );
                    return response($data);
                endif;

                $employeeInfo = Employee::where('employee_id', '=', $request->user_id)->where('employee_status', 1)->first();

                if ($employeeInfo !== null) :
                    if ($request->entrance_time === true && $request->out_time === false) :
                        $employeeEntranceResult = $this->employeeEntrance($request->user_id, $employeeInfo->designation_id, $aiDatas);
                        return response($employeeEntranceResult);
                    endif;

                    if ($request->entrance_time === false && $request->out_time === true) :
                        $employeeOutResult = $this->employeeOut($request->user_id, $employeeInfo->designation_id, $aiDatas);
                        return response($employeeOutResult);
                    endif;
                else :
                    $data = array(
                        'status' => 404,
                        'message' => 'User Not Found'
                    );
                    return response($data);
                endif;
            else :
                $data = array(
                    'status' => 204,
                    'message' => 'No Content'
                );
                return response($data);
            endif;
        else :
            $data = array(
                'status' => 401,
                'message' => 'Unauthorized Request'
            );
            return response($data);
        endif;
    }

    public function employeeEntrance($userId, $designationId, $aiDatas)
    {
        $presentDate = Carbon::now()->format('Y-m-d');
        $entranceTime = $this->checkEntranceDataExist($userId, $presentDate);

        if ($entranceTime === null) :
            $presentTime = Carbon::now()->format('H:i:s');
            $saveEntranceTime = $this->addEntranceTime($userId, $designationId, $presentDate, $presentTime, $aiDatas);

            if ($saveEntranceTime) {
                $data = array(
                    'status' => 201,
                    'message' => 'Data Saved'
                );
                return $data;
            } else {
                $data = array(
                    'status' => 417,
                    'message' => 'Expectation Failed'
                );
                return $data;
            }
        else :
            $data = array(
                'status' => 200,
                'message' => 'Already Added'
            );
            return $data;
        endif;
    }

    public function employeeOut($userId, $designationId, $aiDatas)
    {
        $presentDate = Carbon::now()->format('Y-m-d');
        $entranceTime = $this->checkEntranceDataExist($userId, $presentDate);

        if ($entranceTime === null) :
            $data = array(
                'status' => 406,
                'message' => 'Not Acceptable'
            );
            return $data;
        else :
            $presentTime = Carbon::now()->format('H:i:s');
            $saveOutTime = $this->addOutTime($userId, $presentDate, $presentTime, $aiDatas);

            if ($saveOutTime) {
                $data = array(
                    'status' => 201,
                    'message' => 'Data Saved'
                );
                return $data;
            } else {
                $data = array(
                    'status' => 417,
                    'message' => 'Expectation Failed'
                );
                return $data;
            }
        endif;
    }

    public function checkEntranceDataExist($userId, $date)
    {
        $entranceTime = TimeSchedule::select('entrance_time')->where('employee_id', $userId)->where('date', $date)->orderBy('id', 'DESC')->first();
        return $entranceTime;
    }

    public function addEntranceTime($userId, $designationId, $presentDate, $presentTime, $aiDatas)
    {
        $entranceTime = new TimeSchedule;

        $entranceTime->employee_id          = $userId;
        $entranceTime->designation_id       = $designationId;
        $entranceTime->date                 = $presentDate;
        $entranceTime->entrance_time        = $presentTime;
        $entranceTime->api_entrance_data    = $aiDatas;

        $saveEntranceTime = $entranceTime->save();
        return $saveEntranceTime;
    }

    public function addOutTime($userId, $presentDate, $presentTime, $aiDatas)
    {
        $updateOutTime = TimeSchedule::where('employee_id', $userId)->where('date', $presentDate)->orderBy('id', 'DESC')->first();

        $updateOutTime->out_time        = $presentTime;
        $updateOutTime->api_out_data    = $aiDatas;

        $saveOutTime = $updateOutTime->save();
        return $saveOutTime;
    }
}
