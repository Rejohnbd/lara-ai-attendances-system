<?php

namespace App\Http\Controllers;

use App\Models\TimeSchedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function index()
    {
        $presentDate = Carbon::now()->format('Y-m-d');
        $datas = TimeSchedule::select('employee_id', 'designation_id', 'entrance_time', 'out_time')->where('date', $presentDate)->get();
        return view('pages.attendance.today', compact('datas'));
    }
}
