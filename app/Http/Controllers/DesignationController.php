<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Designation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Designation::all();
        return view('pages.designation.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['designation_slug'] = Str::slug($request->designation);
        $validityRules = $this->validationRules();
        $validator = Validator::make($request->all(), $validityRules);
        $validator->validate();

        $creaeDesignation = Designation::create($request->all());

        if ($creaeDesignation) :
            session()->flash('success', 'Designation Create Successfully');
            return redirect()->route('designation.index');
        else :
            session()->flash('error', 'Something Happened wrong. Try Again');
            return redirect()->back();
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(Designation $designation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Designation $designation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        //
    }

    public function validationRules()
    {
        $rules['designation']       = 'required|string';
        $rules['designation_slug']  = 'required|string|unique:designations';
        return $rules;
    }

    public function attribueNames()
    {
        $attributeNames['designation']      = 'Designation Type is Required';
        $attributeNames['designation_slug'] = 'This Designation is Already Exist';
        return $attributeNames;
    }
}
