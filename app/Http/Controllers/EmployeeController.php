<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\StoreEmployee;
use App\Models\Designation;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('verityDesignationCont')->only(['create', 'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Employee::all();
        return view('pages.employee.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designations = Designation::all();
        return view('pages.employee.create', compact('designations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        $lastEmployeeInfo = Employee::latest()->first();
        if ($lastEmployeeInfo == null) {
            $employeeId = env('EMPLOYEE_FIRST_ID');
        } else {
            $employeeId = $lastEmployeeInfo->employee_id + 1;
        }

        $localAiServer = env('LOCAL_AI_SERVER');
        $empRegiSlug = env('EMP_REGI_API_SLUG');

        try {
            $aiResponse = Http::post($localAiServer . '/' . $empRegiSlug, [
                'company'   => env('COMPANY_SLUG'),
                'user_id'   => $employeeId,
                'file'      => base64_encode(file_get_contents($request->file('employee_image')))
            ]);

            $aiResponseArray = $aiResponse->json();

            if ($aiResponseArray['status'] === 200 && $aiResponseArray['register'] === true) {
                $orginalName =  preg_replace("/\s+/", "_", $request->file('employee_image')->getClientOriginalName());
                $employeeImage = $request->file('employee_image');
                $newEmployee = new Employee;

                $newEmployee->designation_id    = $request->designation_id;
                $newEmployee->employee_id       = $employeeId;
                $newEmployee->employee_name     = $request->employee_name;
                $newEmployee->gender            = $request->gender;
                $newEmployee->employee_image    = $employeeId . '_' . $orginalName;
                $newEmployee->employee_status   = $request->employee_status;

                $saveEmployee = $newEmployee->save();

                $employeeImage->move(storage_path('/app/public/images') . '/', $employeeId . '_' . $orginalName);

                if ($saveEmployee) {
                    session()->flash('success', 'Employee Added Successfully');
                    return redirect()->route('employee.index');
                } else {
                    session()->flash('error', 'Something Happend Wrong');
                    return redirect()->back();
                }
            }

            if ($aiResponseArray['status'] === 500) {
                session()->flash('error', 'Something Happend Wrong');
                return redirect()->back();
            }
        } catch (Exception $error) {
            session()->flash('error', 'Error in AI Server. Try again');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
