<?php

namespace App\Http\Middleware;

use App\Models\Designation;
use Closure;

class VerifyDesignationCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Designation::all()->count() === 0) :
            session()->flash('warning', 'You need to add Designation to create Employee');
            return redirect()->route('designation.create');
        endif;
        return $next($request);
    }
}
