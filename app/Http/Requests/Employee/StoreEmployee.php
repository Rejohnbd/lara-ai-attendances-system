<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name'     => 'required|string',
            'designation_id'    => 'required|numeric',
            'gender'            => 'required|in:1,2',
            'employee_image'    => 'required|mimes:jpeg,jpg,png',
            'employee_status'   => 'required|in:0,1',
        ];
    }

    public function messages()
    {
        return [
            'employee_name.required'    => 'Employee Name Required.',
            'employee_name.string'      => 'Provide Valid Employee Name.',
            'designation_id.string'     => 'Designation Required.',
            'designation_id.numeric'    => 'Provide Valid Designation.',
            'gender.required'           => 'Gender is Required.',
            'gender.in'                 => 'Provide Valid Gender.',
            'employee_image.required'   => 'Employee Image Requided.',
            'employee_image.mimes'      => 'Employee Image must be jpeg/jpg/png.',
            'employee_status.required'  => 'Employee Status is required.',
            'employee_status.in'        => 'Provide Valid Employee Status.',
        ];
    }
}
