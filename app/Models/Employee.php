<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function employee_designation()
    {
        return $this->belongsTo(Designation::class, 'designation_id', 'id');
    }
}
