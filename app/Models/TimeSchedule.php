<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeSchedule extends Model
{
    // 

    /**
     * Return Employee Name
     */
    function findEmployeeName($empId)
    {
        $empInfo = Employee::select('employee_name')->where('employee_id', $empId)->first();
        return $empInfo->employee_name;
    }

    /**
     * Return Employee Designation
     */
    function findEmployeeDepartmentName($deptId)
    {
        $deptInfo = Designation::select('designation')->where('id', $deptId)->first();
        return $deptInfo->designation;
    }
}
