<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('designation_id');
            $table->integer('employee_id')->unique();
            $table->string('employee_name', 50);
            $table->tinyInteger('gender');
            $table->string('employee_image');
            $table->tinyInteger('employee_status');
            $table->timestamps();

            $table->foreign('designation_id')
                ->references('id')
                ->on('designations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
