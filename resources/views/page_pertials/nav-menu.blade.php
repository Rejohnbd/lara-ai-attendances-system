<div class="sticky">
    <div class="horizontal-main hor-menu clearfix ">
        <div class="horizontal-mainwrapper container clearfix">
            <nav class="horizontalMenu clearfix">
                <ul class="horizontalMenu-list">
                    <li aria-haspopup="true"><a href="{{ route('dashboard') }}" class=""><i class="fe fe-grid"></i> Dashboard</a></li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fa fa-group"></i> Employee <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="{{ route('designation.index') }}">Designation</a></li>
                            <li aria-haspopup="true"><a href="{{ route('employee.index') }}"> Employee</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fa fa-group"></i> Attendance <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="{{ route('today-attendance') }}">Today Attendance</a></li>
                            <li aria-haspopup="true"><a href="#"> Attendance Report</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>