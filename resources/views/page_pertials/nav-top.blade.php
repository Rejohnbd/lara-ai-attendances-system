<div class="header top-header hor-topheader" style="z-index: 99;">
    <div id="particles-js" class="zindex1"></div>
    <div class="container">
        <div class="d-flex header-nav">
            <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a>
            <div class="color-headerlogo">
                <a class="header-desktop" href="{{ route('dashboard') }}"></a>
                <a class="header-mobile" href="{{ route('dashboard') }}"></a>
            </div>

            <a class="header-brand header-brand2 d-none d-lg-block  align-items-center justify-content-center" href="{{ route('dashboard') }}">
                <a class="header-desktop" href="{{ route('dashboard') }}"></a>
            </a>
            <div class="d-flex order-lg-2 ml-auto header-right-icons header-search-icon">
                <div class="dropdown  header-fullscreen">
                    <a class="nav-link icon full-screen-link nav-link-bg" id="fullscreen-button">
                        <i class="fe fe-minimize"></i>
                    </a>
                </div>
                <div class="dropdown header-user">
                    <a href="#" class="nav-link icon" data-toggle="dropdown">
                        <span><img src="{{ asset('img/avater.jpg') }}" alt="profile-user" class="avatar brround cover-image mb-0 ml-0"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <div class=" dropdown-header noti-title text-center border-bottom p-3">
                            <div class="header-usertext">
                                <h5 class="mb-1">{{Auth::user()->name}}</h5>
                                <p class="mb-0">{{Auth::user()->email}}</p>
                            </div>
                        </div>
                        <a class="dropdown-item" href="profile.html">
                            <i class="mdi mdi-account-outline mr-2"></i> <span>My profile</span>
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="mdi mdi-settings mr-2"></i> <span>Settings</span>
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="mdi  mdi-logout-variant mr-2"></i> <span>Logout</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>