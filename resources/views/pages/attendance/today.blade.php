@extends('layouts.admin-master')

@section('title', 'Today Attendance')

@section('content')
<div class="container  content-area">
    <div class="section">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}"><i class="fe fe-life-buoy mr-1"></i> Dashboard</a></li>
                <li class="breadcrumb-item" aria-current="page">Today Attendance</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Today Attendance Table</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-vcenter text-nowrap">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Employee Name</th>
                                    <th>Department</th>
                                    <th>In Time</th>
                                    <th>Out Time</th>
                                    <th>Working Hours</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($datas as $data)
                                <tr id="designationTypeId-{{ $data->id }}">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $data->findEmployeeName($data->employee_id) }}</td>
                                    <td>{{ $data->findEmployeeDepartmentName($data->designation_id) }}</td>
                                    <td>@if(is_null($data->entrance_time)) Not Added @else {{ date('h:i A', strtotime($data->entrance_time)) }} @endif</td>
                                    <td>@if(is_null($data->out_time)) Not Added @else {{ date('h:i A', strtotime($data->out_time)) }} @endif</td>
                                    <td>@if(is_null($data->entrance_time) || is_null($data->out_time)) No Calculate Yet @else {{ gmdate("H:i:s" ,(strtotime($data->out_time) - strtotime($data->entrance_time)))}} @endif</td>
                                </tr>
                                @empty
                                <tr>
                                    <th colspan="6" class="text-center">No Data Added Today.</th>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/sweet-alert/sweetalert.css') }}" />
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire('Congratulations!', "{{ session('success') }}", 'success');
    });
</script>
@endif
@endsection