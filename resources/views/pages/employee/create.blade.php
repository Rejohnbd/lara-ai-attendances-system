@extends('layouts.admin-master')

@section('title', 'Create Employee')

@section('content')
<div class="container  content-area">
    <div class="section">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}"><i class="fe fe-life-buoy mr-1"></i> Dashboard</a></li>
                <li class="breadcrumb-item" aria-current="page">Create Employee</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-md">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Create Employee</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('employee.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('Employee Name') }} </label>
                                        <input type="text" class="form-control @error('employee_name') is-invalid @enderror" name="employee_name" placeholder="{{ __('Employee Name') }}" value="{{ old('employee_name') }}" required>
                                        @error('employee_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('Employee Designation') }}</label>
                                        <select name="designation_id" class="form-control @error('designation_id') is-invalid @enderror select2 custom-select" data-placeholder="Choose one" required>
                                            <option label="{{ __('Choose one') }}"></option>
                                            @foreach ($designations as $designation)
                                            <option value="{{$designation->id}}">{{$designation->designation}}</option>
                                            @endforeach
                                        </select>
                                        @error('designation_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label">{{ __('Employee Gender') }}</div>
                                        <div class="custom-controls-stacked form-check-inline">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="gender" value="1" checked required>
                                                <span class="custom-control-label">{{ __('Male') }}</span>
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="gender" value="2">
                                                <span class="custom-control-label">{{ __('Female') }}</span>
                                            </label>
                                        </div>
                                        @error('gender')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label">{{ __('Employee Image') }}</div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="employee_image" accept="image/jpg, image/jpeg, image/png" required>
                                            <label class="custom-file-label">{{ __('Choose file') }}</label>
                                        </div>
                                        @error('employee_image')
                                        <span class="invalid-feedback" role="alert" style="display: block;">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{ __('Employee Status') }}</label>
                                        <select name="employee_status" class="form-control @error('employee_status') is-invalid @enderror select2 custom-select" data-placeholder="Choose one" required>
                                            <option label="Choose one"></option>
                                            <option value="0">{{ __('Inactive Employee') }}</option>
                                            <option value="1">{{ __('Active Employee') }}</option>
                                        </select>
                                        @error('employee_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-block">{{ __('Save Employee') }}</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('employee.index') }}" class="btn btn-warning btn-block">{{ __('Cancel') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/sweet-alert/sweetalert.css') }}" />
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "{{ session('error') }}",
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        })
    });
</script>
@endif
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "{{ session('error') }}",
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        })
    });
</script>
@endif
@endsection